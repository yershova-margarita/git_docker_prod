# Модуль «Автоматизация технологических процессов сборки, настройки и развертывания web-приложений»

## Дополнительные материалы по теме Git
[Открыть методические указания](https://gitlab.com/yershova-margarita/git_docker_prod/-/tree/main/Методические%20указания?ref_type=heads)

## Дополнительные материалы по теме Docker

 [Открыть Wiki](https://gitlab.com/yershova-margarita/git_docker_prod/-/wikis/home)

## Задания к лабораторным работам
[Открыть задания](https://gitlab.com/yershova-margarita/git_docker_prod/-/tree/main/Лабораторные%20работы)
